# AngularJs Skeleton

## Estrutura básica contendo um exemplo

**Pré requisitos**

**Linux**

1. Instalar nodejs (sudo apt-get install nodejs)
2. Instalar npm (sudo apt-get install npm)
3. npm install -g http-server
4. npm install -g bower

### Instalação

1. git clone https://bitbucket.org/angularjsrio/angularjs-skeleton.git
2. npm start

### Pronto AngularJS Instalado e servidor no ar ###